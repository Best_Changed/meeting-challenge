﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DataService } from './components/data.service'
import { AppComponent } from './app.component';
import { IntervalFormComponent } from './components/interval-form.component'

@NgModule({
    imports: [BrowserModule, FormsModule, HttpClientModule],
    declarations: [AppComponent, IntervalFormComponent],
    bootstrap: [AppComponent],
    providers: [DataService]
})
export class AppModule { }
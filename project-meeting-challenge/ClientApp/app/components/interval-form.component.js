var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { scheduleUser } from './scheduleUser';
import { DataService } from "./data.service";
import { END, START } from './intervals';
var IntervalFormComponent = /** @class */ (function () {
    function IntervalFormComponent(dataService) {
        this.dataService = dataService;
        this.invalid_period_text = false; // логическая переменная для контроля неверного ввода периода (буквы или пусто)
        this.invalid_period = false; // логическая переменная для контроля неверного периода (выход за пределы max_interval)
        this._userA = []; //list of classes, ex. [{'title': string, 'start': Date, 'end': Date}, ...]
        this._userB = [];
        this._url = 'assets/data/'; // url к нашим json'ам
        this._free_time = []; //свободное время у обоих 
    }
    IntervalFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.max_interval = (END.getTime() - START.getTime()) / 3600000; // вычисляем наш константый максимальный интервал
        // забираем наши json с сервера и кладем в список классов
        // сразу же один раз вычисляем свободное время у обоих
        this.dataService.requestDataFromMultipleSources(this._url + 'scheduleUserA.json', this._url + 'scheduleUserB.json').subscribe(function (data) {
            _this._userA = data[0];
            _this._userB = data[1];
            for (var _i = 0, _a = _this._userA; _i < _a.length; _i++) {
                var schedule = _a[_i];
                schedule.end = new Date(schedule.end);
                schedule.start = new Date(schedule.start);
            }
            for (var _b = 0, _c = _this._userB; _b < _c.length; _b++) {
                var schedule = _c[_b];
                schedule.end = new Date(schedule.end);
                schedule.start = new Date(schedule.start);
            }
            // ищем все интервалы, которые заняты у обоих персон
            var intervals;
            intervals = _this.search_unavailable_intervals(_this._userA, _this._userB);
            console.log(intervals);
            // вычисляем все интервалы, которые свободны у обоих персон на константном интервале
            _this._free_time = _this.search_available_intervals(START, END, intervals);
            console.log(_this._free_time);
        });
    };
    ;
    IntervalFormComponent.prototype.search_unavailable_intervals = function (userA, userB) {
        var intervals = [];
        intervals = userA.concat(userB); // объеденяем все в один массив
        intervals.sort(function (a, b) {
            if (a.start > b.start) {
                return 1;
            }
            if (a.start < b.start) {
                return -1;
            }
            return 0;
        });
        var result = [];
        intervals.forEach(function (val, index, ar) {
            if (index == 0) {
                result.push(val);
            }
            else {
                if (result[result.length - 1].end >= val.start) {
                    result[result.length - 1].end = val.end;
                }
                else {
                    result.push(val);
                }
            }
        });
        return result;
    };
    IntervalFormComponent.prototype.search_available_intervals = function (start, end, intervals) {
        var result = [];
        var current_time = start;
        intervals.forEach(function (val, ind, ar) {
            var help_var = new scheduleUser();
            if (current_time < val.start) {
                help_var.start = current_time;
                help_var.end = val.start;
                result.push(help_var);
            }
            if (ind == ar.length - 1) {
                if (val.end < end) {
                    help_var.start = val.end;
                    help_var.end = end;
                    result.push(help_var);
                }
            }
            current_time = val.end; // смещаем счетчик
        });
        return result;
    };
    IntervalFormComponent.prototype.find_result_intervals = function (mins) {
        function prepare_hm(hm) {
            var result;
            result = hm;
            if (hm.length == 1) {
                result = '0' + hm;
            }
            return result;
        }
        var result = [];
        this._free_time.forEach(function (val) {
            if (((val.end.getTime() - val.start.getTime()) / 60000) >= mins) {
                var interval = '';
                var start = prepare_hm(val.start.getHours().toString()) + ':' + prepare_hm(val.start.getMinutes().toString());
                var end = prepare_hm(val.end.getHours().toString()) + ':' + prepare_hm(val.end.getMinutes().toString());
                interval = start + ' - ' + end;
                result.push(interval);
            }
        });
        return result;
    };
    IntervalFormComponent.prototype.OnClick_Search = function (f) {
        this.result = []; // очищаем результаты
        this.invalid_period = false; // обновляем валидаторы
        this.invalid_period_text = false; // обновляем валидаторы
        var valid = f.valid;
        if (valid) { // если форма заполнена
            var mins = Number(f.value.period);
            if (mins) { // если успешно конверитровали в минуты проверим выходят ли они за константный предел
                if (mins > (END.getTime() - START.getTime()) / 60000) { // вышли за константный интервал
                    this.invalid_period = true;
                }
                else { // ищем свободные интервалы соответсвующие не меньше введеных минут
                    this.result = this.find_result_intervals(mins);
                }
            }
            else { // ошибка введены не цифры
                this.invalid_period_text = true;
            }
        }
        else { // форма не заполнена - отобразим ошибку
            this.invalid_period_text = true;
        }
    };
    IntervalFormComponent = __decorate([
        Component({
            selector: 'app-interval-form',
            templateUrl: '../templates/interval-form.component.html'
        }),
        __metadata("design:paramtypes", [DataService])
    ], IntervalFormComponent);
    return IntervalFormComponent;
}());
export { IntervalFormComponent };
//# sourceMappingURL=interval-form.component.js.map
﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

import { scheduleUser } from './scheduleUser';

@Injectable()
export class DataService {

    constructor(private http: HttpClient) { }
    // TODO: заменить два string параметра на массив строк. Так можно будет выполнять любое кол-во запросов одновременно
    public requestDataFromMultipleSources(requestUrl1: string, requestUrl2: string): Observable<any []> {
        // Делаем сразу два запроса
        let response1 = this.http.get(requestUrl1);
        let response2 = this.http.get(requestUrl2);
        return forkJoin([response1, response2]);
    }
}
﻿import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { scheduleUser } from './scheduleUser';
import { DataService } from "./data.service";
import { END, START } from './intervals';


@Component({
    selector: 'app-interval-form',
    templateUrl: '../templates/interval-form.component.html'
})

export class IntervalFormComponent implements OnInit {

    public invalid_period_text: boolean = false;    // логическая переменная для контроля неверного ввода периода (буквы или пусто)
    public invalid_period: boolean = false;         // логическая переменная для контроля неверного периода (выход за пределы max_interval)
    public max_interval: number;                    // константный интервал в часах

    private _userA: scheduleUser[]=[];   //list of classes, ex. [{'title': string, 'start': Date, 'end': Date}, ...]
    private _userB: scheduleUser[]=[];
    private _url: string = 'assets/data/';  // url к нашим json'ам
    public result: string[];   // все интервалы соответствующие выбранной длительности
    private _free_time: scheduleUser[] = [];    //свободное время у обоих 

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.max_interval = (END.getTime() - START.getTime()) / 3600000; // вычисляем наш константый максимальный интервал
        // забираем наши json с сервера и кладем в список классов
        // сразу же один раз вычисляем свободное время у обоих
        this.dataService.requestDataFromMultipleSources(this._url + 'scheduleUserA.json', this._url + 'scheduleUserB.json').subscribe((data: Array<Array<scheduleUser>>) => {
            this._userA = data[0];
            this._userB = data[1];
            for (let schedule of this._userA) {
                schedule.end = new Date(schedule.end);
                schedule.start = new Date(schedule.start);
            }
            for (let schedule of this._userB) {
                schedule.end = new Date(schedule.end);
                schedule.start = new Date(schedule.start);
            }
            // ищем все интервалы, которые заняты у обоих персон
            let intervals: scheduleUser[];
            intervals = this.search_unavailable_intervals(this._userA, this._userB);
            console.log(intervals);
            // вычисляем все интервалы, которые свободны у обоих персон на константном интервале
            this._free_time = this.search_available_intervals(START, END, intervals);
            console.log(this._free_time);
        });
    };

    private search_unavailable_intervals(userA: scheduleUser[], userB: scheduleUser[]): scheduleUser[] {
        let intervals: scheduleUser[] = [];
        intervals = userA.concat(userB); // объеденяем все в один массив
        intervals.sort((a, b) => { // сортируем массив дат по началу встреч
            if (a.start > b.start) {
                return 1;
            }

            if (a.start < b.start) {
                return -1;
            }
            return 0;
        })
        let result: scheduleUser[] = [];
        intervals.forEach(function (val, index, ar) { // склеиваем пересекающиеся интервалы в один
            if (index == 0) {
                result.push(val);
            }
            else {
                if (result[result.length - 1].end >= val.start) {
                    result[result.length - 1].end = val.end;
                }
                else {
                    result.push(val);
                }
            }
        })
        return result;
    }

    private search_available_intervals(start: Date, end: Date, intervals: scheduleUser[]): scheduleUser[] {
        let result: scheduleUser[] = [];
        let current_time: Date = start;
        intervals.forEach(function (val, ind, ar) { // находим все свободные интервалы со START до END
            let help_var: scheduleUser = new scheduleUser();
            if (current_time < val.start) {
                help_var.start = current_time;
                help_var.end = val.start;
                result.push(help_var);
            }
            if (ind == ar.length - 1) {
                if (val.end < end) {
                    help_var.start = val.end;
                    help_var.end = end;
                    result.push(help_var);
                }
            }
            current_time = val.end; // смещаем счетчик
        });
        return result;
    }

    private find_result_intervals(mins: number): string[] {
        function prepare_hm(hm: string): string {   // делает из одной цифры часов или минут две (вместо 0 - 00, 6 - 06)
            let result: string;
            result = hm;
            if (hm.length == 1) {
                result = '0' + hm;
            }
            return result;
        }
        let result: string[] = [];
        this._free_time.forEach(function (val) {
            if (((val.end.getTime() - val.start.getTime()) / 60000) >= mins) {
                let interval: string = '';
                let start: string = prepare_hm(val.start.getHours().toString()) + ':' + prepare_hm(val.start.getMinutes().toString());
                let end: string = prepare_hm(val.end.getHours().toString()) + ':' + prepare_hm(val.end.getMinutes().toString());
                interval = start + ' - ' + end;
                result.push(interval);
            }
        });
        return result;
    }

    OnClick_Search(f: NgForm) {
        this.result = [];                   // очищаем результаты
        this.invalid_period = false;        // обновляем валидаторы
        this.invalid_period_text = false;   // обновляем валидаторы
        let valid: boolean = f.valid;
        if (valid) {                        // если форма заполнена
            let mins: number = Number(f.value.period);
            if (mins) {                     // если успешно конверитровали в минуты проверим выходят ли они за константный предел
                if (mins > (END.getTime() - START.getTime()) / 60000) { // вышли за константный интервал
                    this.invalid_period = true;
                }
                else {  // ищем свободные интервалы соответсвующие не меньше введеных минут
                    this.result = this.find_result_intervals(mins);
                }
            }
            else { // ошибка введены не цифры
                this.invalid_period_text = true;
            }
        }
        else { // форма не заполнена - отобразим ошибку
            this.invalid_period_text = true;
        }

    }
}
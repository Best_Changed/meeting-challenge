var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs'; // RxJS 6 syntax
var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
    }
    // TODO: заменить два string параметра на массив строк. Так можно будет выполнять любое кол-во запросов одновременно
    DataService.prototype.requestDataFromMultipleSources = function (requestUrl1, requestUrl2) {
        // Делаем сразу два запроса
        var response1 = this.http.get(requestUrl1);
        var response2 = this.http.get(requestUrl2);
        return forkJoin([response1, response2]);
    };
    DataService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=data.service.js.map